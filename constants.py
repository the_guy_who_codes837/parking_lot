# All the project wide constants are defined here


class ErrorMsg:
    SLOT_ALREADY_BOOKED = (
        "This slot is already booked. Please try to "
        "book a free slot."
    )
    INVALID_SLOT_ID = "Invalid slot id."
    VEHICLE_ALREADY_PARKED = "Vehicle is already parked."
    NO_SLOT_AVAILABLE = (
        "No slot is available at the moment."
    )
    SLOT_ALREADY_FREE = (
        "Can not free an already available slot."
    )
    NO_VEHICLE_PARKED = "No such vehicle parked."
    NO_VEHICLE_PARKED_BY_AGE = "No vehicles parked"


class InputCommandPattern:
    VEHICLE_ID = "[A-Z]{2}-\d{2}-[A-Z]{2}-\d{4}" # noqa
    NUMBER = "\d+" # noqa
    VEHICLES_BY_AGE = f"Vehicle_registration_number_for_driver_of_age {NUMBER}"
    CREATE_PARKING_LOT = f"Create_parking_lot {NUMBER}"
    SLOTS_BY_AGE = f"Slot_numbers_for_driver_of_age {NUMBER}"
    LEAVE_SLOT = f"Leave {NUMBER}"
    PARK_VEHICLE = f"Park {VEHICLE_ID} driver_age {NUMBER}"
    SLOT_BY_VEHICLE = f"Slot_number_for_car_with_number {VEHICLE_ID}"


class CommandParseParams:
    VEHICLES_BY_AGE = [InputCommandPattern.NUMBER]
    CREATE_PARKING_LOT = [InputCommandPattern.NUMBER]
    SLOTS_BY_AGE = [InputCommandPattern.NUMBER]
    LEAVE_SLOT = [InputCommandPattern.NUMBER]
    PARK_VEHICLE = [InputCommandPattern.VEHICLE_ID, InputCommandPattern.NUMBER]
    SLOT_BY_VEHICLE = [InputCommandPattern.VEHICLE_ID]
