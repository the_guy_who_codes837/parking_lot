# defines all the custom exception classes


class SlotAlreadyBooked(Exception):
    pass


class InvalidSlotId(Exception):
    pass


class VehicleAlreadyParked(Exception):
    pass


class NoSlotAvailable(Exception):
    pass


class SlotAlreadyFree(Exception):
    pass


class NoVehicleParked(Exception):
    pass


class InvalidInputCommand(Exception):
    pass
