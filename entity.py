# All the entities and business logic associated with
# those enitites are defined here.


# standard imports
import datetime


# project level imports
from exceptions import (
    SlotAlreadyBooked, SlotAlreadyFree, InvalidSlotId,
    VehicleAlreadyParked, NoSlotAvailable, NoVehicleParked
)
from constants import ErrorMsg


class Slot:
    def __init__(self, slot_id):
        self.slot_id = slot_id
        self.available = True
        self.ticket = None

    def __repr__(self) -> str:
        return f"{self.slot_id}"


class ParkingLot:
    def __init__(self, total_slots):
        self.total_slots = total_slots
        self._slots = [Slot(slot_id) for slot_id in range(1, total_slots + 1)]

    def get_free_slot(self):
        """Returns the first available free slot. In case this returns None,
        that means all the slots are booked. Business logic should raise the
        appropriate error in such case.

        Note: This method is not concurrency safe. Locking should be used to
        handle such cases.
        """
        for slot in self._slots:
            if slot.available:
                return slot

    def get_occupied_slots(self):
        """Returns the list of all occupied slots"""
        return [slot for slot in self._slots if not slot.available]

    def book_slot(self, slot, ticket):
        if not slot.available:
            raise SlotAlreadyBooked
        slot.available = False
        slot.ticket = ticket

    def free_slot(self, slot):
        slot.ticket = None
        slot.available = True

    def get_slot_by_id(self, slot_id):
        if 1 <= slot_id <= self.total_slots:
            return self._slots[slot_id - 1]

    def free_slot_by_id(self, slot_id):
        slot = self.get_slot_by_id(slot_id)
        if not slot:
            raise InvalidSlotId(ErrorMsg.INVALID_SLOT_ID)
        self.free_slot(slot)
        return slot


class Ticket:
    def __init__(self, vehicle_id, age, slot):
        self.vehicle_id = vehicle_id
        self.age = age
        self.slot = slot
        self.as_on = datetime.datetime.utcnow()
        self._expired = False

    def expire(self):
        self._expired = True


class TicketCounter:
    def setup(self, total_slots):
        self.plot = ParkingLot(total_slots)

        # using maps for faster lookups
        self._age_ticket_map = {}
        self._vehicle_ticket_map = {}

        # not really needed as per the requirements
        self._expired_tickets_entry = []

    def _add_map_entry(self, ticket):
        age_ticket_set = self._age_ticket_map.get(ticket.age, set())
        age_ticket_set.add(ticket)
        self._age_ticket_map[ticket.age] = age_ticket_set
        self._vehicle_ticket_map[ticket.vehicle_id] = ticket

    def _remove_map_entry(self, ticket):
        age_ticket_set = self._age_ticket_map.get(ticket.age, set())
        if ticket in age_ticket_set:
            age_ticket_set.remove(ticket)
        self._age_ticket_map[ticket.age] = age_ticket_set
        self._vehicle_ticket_map.pop(ticket.vehicle_id, None)

    def _expire_ticket(self, ticket):
        ticket.expire()
        self._expired_tickets_entry.append(ticket)

    def park(self, vehicle_id, age):
        if self._vehicle_ticket_map.get(vehicle_id):
            raise VehicleAlreadyParked
        slot = self.plot.get_free_slot()
        if not slot:
            raise NoSlotAvailable
        ticket = Ticket(vehicle_id, age, slot)
        self.plot.book_slot(slot, ticket)
        self._add_map_entry(ticket)
        return ticket

    def leave_slot(self, slot_id):
        slot = self.plot.get_slot_by_id(slot_id)
        if not slot:
            raise InvalidSlotId
        if slot.available:
            raise SlotAlreadyFree
        ticket = slot.ticket
        self.plot.free_slot(slot)
        self._remove_map_entry(ticket)
        self._expire_ticket(ticket)
        return ticket

    def get_slots_by_age(self, age):
        tickets = self._age_ticket_map.get(age)
        if not tickets:
            raise NoVehicleParked
        return [ticket.slot for ticket in tickets]

    def get_slot_by_vehicle_id(self, vehicle_id):
        ticket = self._vehicle_ticket_map.get(vehicle_id)
        if not ticket:
            raise NoVehicleParked
        return ticket.slot

    def get_vehicles_by_age(self, age):
        tickets = self._age_ticket_map.get(age)
        if not tickets:
            raise NoVehicleParked
        return [ticket.vehicle_id for ticket in tickets]
