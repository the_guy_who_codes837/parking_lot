# standard imports
import unittest


# project level imports
from entity import TicketCounter, ParkingLot, Slot
import exceptions


class TestTicketCounterMethods(unittest.TestCase):
    def test_setup(self):
        tc = TicketCounter()
        tc.setup(5)
        assert isinstance(tc.plot, ParkingLot)
        assert isinstance(tc._age_ticket_map, dict)
        assert isinstance(tc._vehicle_ticket_map, dict)
        assert isinstance(tc._expired_tickets_entry, list)

    def test_park_success(self):
        tc = TicketCounter()
        tc.setup(2)
        ticket = tc.park("vehicle1", 21)
        assert ticket.vehicle_id == "vehicle1"
        assert ticket.age == 21
        assert isinstance(ticket.slot, Slot)
        assert ticket.slot.slot_id == 1
        assert tc._age_ticket_map == {21: {ticket, }}
        assert tc._vehicle_ticket_map == {"vehicle1": ticket}

    def test_park_vehicle_already_parked(self):
        tc = TicketCounter()
        tc.setup(5)
        tc.park("vehicle1", 21)

        # since, vehicle1 is already parked. Any attempt to
        # park this vehicle again should raise an VehicleAlreadyParked
        # exception.
        self.assertRaises(
            exceptions.VehicleAlreadyParked,
            tc.park, "vehicle1", 21
        )

    def test_park_no_slot_available(self):
        tc = TicketCounter()
        tc.setup(1)
        tc.park("vehicle1", 21)

        # At this point, no slot is available in ParkingLot.
        # So, trying to park an vehicle again should raise an
        # NoSlotAvailable exception.
        self.assertRaises(
            exceptions.NoSlotAvailable,
            tc.park, "vehicle2", 21
        )

    def test_get_slots_by_age(self):
        tc = TicketCounter()
        tc.setup(20)
        tc.park("car1", 21)
        tc.park("car2", 21)
        tc.park("car3", 23)
        tc.park("car4", 23)
        tc.park("car5", 21)

        slots_age_21 = [
            slot.slot_id for slot in
            tc.get_slots_by_age(21)
        ]
        # since there is no ordering guarantee in set, we need
        # to sort it first.
        assert sorted(slots_age_21) == [1, 2, 5]

        slots_age_23 = [
            slot.slot_id for slot in
            tc.get_slots_by_age(23)
        ]
        assert sorted(slots_age_23) == [3, 4]

    def test_get_slot_by_vehicle_id(self):
        tc = TicketCounter()
        tc.setup(5)
        tc.park("car1", 21)
        tc.park("car2", 21)
        tc.park("car3", 24)
        tc.park("car4", 24)
        assert tc.get_slot_by_vehicle_id("car1").slot_id == 1
        assert tc.get_slot_by_vehicle_id("car2").slot_id == 2
        assert tc.get_slot_by_vehicle_id("car3").slot_id == 3
        assert tc.get_slot_by_vehicle_id("car4").slot_id == 4

        # since car8 is not parked, calling the method on it will
        # raise a NoVehicleParked exception.
        self.assertRaises(
            exceptions.NoVehicleParked,
            tc.get_slot_by_vehicle_id, "car8"
        )

    def test_get_vehicles_by_age(self):
        tc = TicketCounter()
        tc.setup(5)
        tc.park("car1", 21)
        tc.park("car2", 21)
        tc.park("car3", 23)
        tc.park("car4", 23)
        tc.park("car5", 21)

        vehicles_age_21 = tc.get_vehicles_by_age(21)
        assert sorted(vehicles_age_21) == ["car1", "car2", "car5"]

        vehicles_age_23 = tc.get_vehicles_by_age(23)
        assert sorted(vehicles_age_23) == ["car3", "car4"]

    def test_leave_slot(self):
        tc = TicketCounter()

        tc.setup(5)

        assert tc.plot.get_free_slot().slot_id == 1
        tc.park("car1", 21)
        tc.park("car2", 21)

        ticket = tc.leave_slot(1)
        assert ticket.slot.ticket is None
        assert ticket._expired

        assert tc.plot.get_free_slot().slot_id == 1
