# standard imports
import sys


# project level imports
import controllers


def read_file_commands(filename):
    """This method will read commands from the file line by line.

    Since, we are reading from file line by line, a geberator method is
    apt here. Otherwise, all the content of files would have been loaded
    into the memory at once.
    """
    with open(filename) as reader:
        line = reader.readline()
        while line != '':
            yield line
            line = reader.readline()


def run_file(filename):
    commands = read_file_commands(filename)
    for output in controllers.execute(commands):
        print(output)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        exit("Incorrect number of arguments")
    filename = sys.argv[1]
    run_file(filename)
