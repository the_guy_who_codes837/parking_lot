# We need to make sure that methods defined here are independent of
# I/O source.If input source changes from file tp something
# else, methods defined here should not change.


# standard import
import re


# project level imports
from constants import (
    InputCommandPattern, CommandParseParams, ErrorMsg
)
from entity import TicketCounter
import exceptions


# controller function on top of methods defined in entity.TicketCounter

def setup(tc, total_slots):
    tc.setup(total_slots)
    return f"Created parking of {total_slots} slots"


def park(tc, vehicle_id, age):
    try:
        ticket = tc.park(vehicle_id, age)
        return (
            f"Car with vehicle registration number {ticket.vehicle_id} "
            f"has been parked at slot number {ticket.slot.slot_id},"
            f" age of the driver is {ticket.age}"
        )
    except exceptions.VehicleAlreadyParked:
        return ErrorMsg.VEHICLE_ALREADY_PARKED
    except exceptions.NoSlotAvailable:
        return ErrorMsg.NO_SLOT_AVAILABLE
    except exceptions.SlotAlreadyBooked:
        return ErrorMsg.SLOT_ALREADY_BOOKED


def leave_slot(tc, slot_id):
    try:
        ticket = tc.leave_slot(slot_id)
        return (
            f"Slot number {ticket.slot.slot_id} vacated, the car "
            f"with vehicle registration number {ticket.vehicle_id} "
            f"left the space, the drive of the car was of age {ticket.age}"
        )
    except exceptions.InvalidSlotId:
        return ErrorMsg.INVALID_SLOT_ID
    except exceptions.SlotAlreadyFree:
        return ErrorMsg.SLOT_ALREADY_FREE


def slots_by_age(tc, age):
    try:
        slots = tc.get_slots_by_age(age)
        slot_ids = [str(slot.slot_id) for slot in slots]
        return ", ".join(slot_ids)
    except exceptions.NoVehicleParked:
        return ErrorMsg.NO_VEHICLE_PARKED_BY_AGE


def slots_by_vehicle_id(tc, vehicle_id):
    try:
        slot = tc.get_slot_by_vehicle_id(vehicle_id)
        return slot.slot_id
    except exceptions.NoVehicleParked:
        return ErrorMsg.NO_VEHICLE_PARKED


def get_vehicles_by_age(tc, age):
    try:
        vehicles = tc.get_vehicles_by_age(age)
        return ", ".join(vehicles)
    except exceptions.NoVehicleParked:
        return ErrorMsg.NO_VEHICLE_PARKED_BY_AGE


def parse_command_params(command, arg_patterns):
    """Returns the argument values in the command.

    Input:
        command(str) - Input command to be parsed.
        arg_patterns - regex pattern for the argument in the order they appear.

    Returns:
        List<str> - returns the parsed arguments

    Example - In the command "Park KA-01-HH-1234 driver_age 21",
    this method will parse the literal values(KA-01-HH-1234, 21) and
    return it.
    """
    args = []
    for pattern in arg_patterns:
        val = re.findall(pattern, command)[0]
        command = command.replace(val, "")
        if pattern == InputCommandPattern.NUMBER:
            val = int(val)
        args.append(val)
    return args


def fetch_command_method_and_arg_patterns(command):
    """This method fetches the method to be called and
    appropriate pattern for the arguments passed in the command.
    A regex pattern is used to map the commands to its method and
    arg patterns. In case no regex check evaluates to True, this method
    will raise an InvalidInputCommand exception.

    Input:
        command(str) - Input command

    Returns:
        Tuple<Function, List<Str>> - Returns a tuple of Function associated
        with the command and argument pattern to parse the arguments.
    """
    if re.match(InputCommandPattern.CREATE_PARKING_LOT, command):
        arg_patterns = CommandParseParams.CREATE_PARKING_LOT
        method = setup

    elif re.match(InputCommandPattern.VEHICLES_BY_AGE, command):
        arg_patterns = CommandParseParams.VEHICLES_BY_AGE
        method = get_vehicles_by_age

    elif re.match(InputCommandPattern.SLOTS_BY_AGE, command):
        arg_patterns = CommandParseParams.SLOTS_BY_AGE
        method = slots_by_age

    elif re.match(InputCommandPattern.LEAVE_SLOT, command):
        arg_patterns = CommandParseParams.LEAVE_SLOT
        method = leave_slot

    elif re.match(InputCommandPattern.PARK_VEHICLE, command):
        arg_patterns = CommandParseParams.PARK_VEHICLE
        method = park

    elif re.match(InputCommandPattern.SLOT_BY_VEHICLE, command):
        arg_patterns = CommandParseParams.SLOT_BY_VEHICLE
        method = slots_by_vehicle_id

    else:
        msg = f"command[{command}] is not supported"
        raise exceptions.InvalidInputCommand(msg)
    return method, arg_patterns


def parse_command(command):
    """This function will return the method and arguments associated with
    the command.

    Input:
        command(str) - Input command

    Returns:
        Tuple<Function, List<str>> - returns a tuple of method and arguments.
    """
    command = command.strip()
    method, arg_patterns = fetch_command_method_and_arg_patterns(command)
    args = parse_command_params(command, arg_patterns)
    return method, args


def execute(commands):
    """Executes the commands passed to this method as an iterator.

    Input:
        commands(Iter<str>)
    """
    tc = TicketCounter()
    for command in commands:
        method, args = parse_command(command)
        output = method(tc, *args)
        yield output
