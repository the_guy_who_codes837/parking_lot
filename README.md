This project does not require any external library. So requirements.txt is not included.

# Project Structure
```
.
├── README.md 
├── constants.py - All the constants are defined here.
├── controllers.py - Name speaks for itself. I have tried very hard to make this independent of I/O source.
├── entity.py - Models + core logic 
├── exceptions.py - Defines all the custom exceptions
├── executor.py - This is the entry point to execute this project
├── input.txt - Sample input file
└── test.py - All the tests go here.
```

# Steps to run the project

run `$ python executor.py filename` in the terminal.

```bash
$ python executor.py input.txt
Created parking of 6 slots
Car with vehicle registration number KA-01-HH-1234 has been parked at slot number 1, age of the driver is 21
Car with vehicle registration number PB-01-HH-1234 has been parked at slot number 2, age of the driver is 21
2, 1
Car with vehicle registration number PB-01-TG-2341 has been parked at slot number 3, age of the driver is 40
2
Slot number 2 vacated, the car with vehicle registration number PB-01-HH-1234 left the space, the drive of the car was of age 21
Car with vehicle registration number HR-29-TG-3098 has been parked at slot number 2, age of the driver is 39
```

# Steps to run the tests
run `$ python -m unittest test.py` in the terminal

```bash
 $ python -m unittest test.py
 ........
----------------------------------------------------------------------
Ran 8 tests in 0.001s

OK
```

# Notes
1.  This project runs in a single thread. There are some core operations which will not run as expected if we introduce concurrency.
2.  I have only written tests for the TicketCounter Method, as all the core business logic is here. Ideally each and every method should be unit tested.
3.  I have commited everything at once. Not a great practice.
4.  I have not used logging. In a production system, that should be the first thing which one should set up.
